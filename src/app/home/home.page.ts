import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { QuestionPage } from '../question/question.page';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  // Member variables

 
  constructor( public router: Router) {}
  
  // when page loads, add data to questions array
  ngOnInit() {
   
  } //end off ngOnInit

  start() {
    this.router.navigateByUrl('question/')
  } //end of start
} // end off HomePage
