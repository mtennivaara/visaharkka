import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResultPage } from '../result/result.page';
import { HarjQuestion } from '../../harjquestions';
import { Harj } from 'src/harj';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {
  questions: HarjQuestion []=[];
  activeQuestion: HarjQuestion;
  isCorrect: boolean;
  feedback: string;
  questionCounter: number;
  correctAnsCounter = 0;
  optionCounter: number;
  startTime: Date;
  endTime: Date;
  duration: number;

  constructor(public router: Router, public activatedRoute: ActivatedRoute) { }

ngOnInit() {
  this.questionCounter = 0;
  this.correctAnsCounter = 0;
  this.startTime = new Date;

  fetch('../../assets/data/questions.json').then(res => res.json())
  .then(json => {
  this.questions = json;
  this.activeQuestion = this.questions[this.questionCounter];
  this.setQuestion();  
  }); 
} // end off ngOnInit

setQuestion() {
  if (this.questionCounter === this.questions.length) {
    this.endTime = new Date();
    this.duration = this.endTime.getTime() - this.startTime.getTime();
    this.router.navigateByUrl('result/') +'/' + this.duration + '/' + this.questionCounter 
    + '/' + this.correctAnsCounter;
  } else {
    this.isCorrect = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
  }
} // end off setQuestion

}

